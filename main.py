import argparse, warnings
from configparser import ConfigParser

import numpy as np
from matplotlib import pyplot as plt
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from nltk.classify import NaiveBayesClassifier
import nltk
from sklearn.metrics import accuracy_score
from time import time

from utils.twitterapi import TwitterAPI
from utils.TweetParser import TweetParser
from utils.Dataset import Dataset
from utils.Lexicons import TwITALexicon, GenericLexicon
from utils.exceptions import IllegalArgumentError
from utils.functions import *

# List of available classifiers
classifiers = ["logistic_tfidf", "logistic", "nb", "knn", "knn_tfidf", "kmeans", "all"]

consumer_key = consumer_key_secret = access_token = access_token_secret = None
product = development_label = None
lexicon = twitter_api = tweet_parser = None

be_verbose = False

config_file = "config.conf"
dataset_path = "Dataset/tweets/sentipol16_dataset.csv"


def logistic_classifier(training_dataset, test_dataset, data_to_classify, query, verbose=False):
    if not isinstance(training_dataset, Dataset):
        raise IllegalArgumentError("training_dataset should be a Dataset object")
    elif not isinstance(test_dataset, Dataset):
        raise IllegalArgumentError("test_dataset should be a Databaset object")
    elif not isinstance(data_to_classify, list):
        raise IllegalArgumentError("data_to_classify should be a list object")

    print("*******************************\nRunning Logistic Classifier\n*******************************")

    lreg = LogisticRegression(multi_class="multinomial", solver="lbfgs", class_weight="balanced")

    if verbose:
        print("Step: TRAINING.",
              "\n\tTraining dataset has {:d} samples.".format(training_dataset.get_length()))

    training_x = np.array([[data["pos_score"], data["neg_score"], data["score"]] for data in training_dataset.data["scores"]])
    training_y = np.array(training_dataset.data["labels"])

    if verbose:
        print("\tBegin training.")

    start_time, end_time = 0, 0
    start_time = time()
    lreg.fit(training_x, training_y)
    end_time = time()

    if verbose:
        print("\tCompleted in {:.2f}s".format(end_time-start_time))

    if verbose:
        print("\n\nStep: TEST.",
              "\n\tTest dataset has {:d} samples.".format(test_dataset.get_length()))

    test_x = np.array([[data["pos_score"], data["neg_score"], data["score"]] for data in test_dataset.data["scores"]])
    test_y = np.array(test_dataset.data["labels"])

    if verbose:
        print("\tBegin testing.")

    start_time = time()
    predicted_test_y = lreg.predict(test_x)
    end_time = time()

    if verbose:
        print("\tDone in {:.2f}s".format(end_time-start_time))
        print("\n****\nLogistic classifier accuracy: {:f}\n****".format(accuracy_score(test_y, predicted_test_y)))

    # Classifing data
    if verbose:
        print("\n\nStep: CLASSIFICATION.",
              "\n\t{:d} tweets to classify.".format(len(data_to_classify)))

    scores = np.array([[data["pos_score"], data["neg_score"], data["score"]]
                       for data in [training_dataset.get_tweet_score(tokens) for tokens in data_to_classify]])

    if verbose:
        print("\tBegin classification.")
    start_time = time()
    results = lreg.predict(scores)
    end_time = time()
    if verbose:
        print("\tCompleted in {:.2f}s".format(end_time-start_time))
        print("\nPlotting data...")

    show_bar_pie_charts(results, query)
    show_histograms_bar_charts(scores, query)
    show_xy_score_distribution(scores, query)

    plt.show()


def logistic_classifier_tfidf(training_dataset, test_dataset, data_to_classify, query, verbose=False):
    if not isinstance(training_dataset, Dataset):
        raise IllegalArgumentError("training_dataset should be a Dataset object")
    elif not isinstance(test_dataset, Dataset):
        raise IllegalArgumentError("test_dataset should be a Databaset object")
    elif not isinstance(data_to_classify, list):
        raise IllegalArgumentError("data_to_classify should be a list object")

    print("*******************************\nRunning Logistic Classifier based on TF-IDF\n*******************************")

    # Term Frequency - Inverse Document Frequency Vectorizer:
    #  - 'word_vec' works on ngram build up from words;
    #  - 'char_vec' works on ngram builded up from char set
    word_vec = TfidfVectorizer(ngram_range=(1, 1), analyzer='word', min_df=0.05, max_df=0.9,
                               vocabulary=lexicon.get_words_list(), stop_words=lexicon.get_stopwords(),
                               tokenizer=None)
    char_vec = TfidfVectorizer(ngram_range=(2, 8), analyzer='char', min_df=0.05, max_df=0.9,
                               vocabulary=lexicon.get_words_list(), stop_words=lexicon.get_stopwords(),
                               tokenizer=None)

    # Logistic Regression objects, one for each vectorizer
    lreg_word = LogisticRegression(C=400, multi_class="multinomial", solver="lbfgs", max_iter=5000, class_weight="balanced")
    lreg_char = LogisticRegression(C=400, multi_class="multinomial", solver="lbfgs", max_iter=5000, class_weight="balanced")

    time_interval: float = 0

    if verbose:
        print("Step: TRAINING.",
              "\n\tTraining dataset has {:d} samples.".format(training_dataset.get_length()))
        print("\tBegin training.")


    # Training
    time_interval = time()  # Start timer

    # Fit everything both in char and word vectorizer
    char_vec.fit(training_dataset.data["corpus"] + test_dataset.data["corpus"])
    word_vec.fit(training_dataset.data["corpus"] + test_dataset.data["corpus"])

    # Get TF-IDF BoW
    features_char_x = char_vec.transform(training_dataset.data["corpus"])
    features_word_x = word_vec.transform(training_dataset.data["corpus"])
    training_y = training_dataset.data["labels"]  # Set Y

    # Training logistic regression both for word and char model
    lreg_word.fit(features_word_x, training_y)
    lreg_char.fit(features_char_x, training_y)

    time_interval = time() - time_interval

    if verbose:
        print("\tCompleted in {:.2f}s".format(time_interval))

    # Test
    if verbose:
        print("\n\nStep: TEST.",
              "\n\tTest dataset has {:d} samples.".format(test_dataset.get_length()))
        print("\tBegin testing.")

    time_interval = time()  # Start timer

    # Get document-term tfidf matrix
    x_test_word = word_vec.transform(test_dataset.data["corpus"])
    x_test_char = char_vec.transform(test_dataset.data["corpus"])

    # Get probabilities distribution both for word and char models
    test_prob_predict_word = lreg_word.predict_proba(x_test_word)
    test_prob_predict_char = lreg_char.predict_proba(x_test_char)

    test_y = test_dataset.data["labels"]  # Set Y labels for testing

    # Get predicted class from probability distribution
    test_class_predict_word = get_classes_from_probs(test_prob_predict_word, lreg_word.classes_)
    test_class_predict_char = get_classes_from_probs(test_prob_predict_char, lreg_char.classes_)

    # make a combined model to maximize probability
    test_prob_predict_combined = list()
    for index, probs in enumerate(test_prob_predict_word):
        prob_char = test_prob_predict_char[index]
        test_prob_predict_combined.append([(x+y)*0.5 for (x, y) in zip(probs, prob_char)])

    # Get class names from combined probability
    test_class_predict_combined = get_classes_from_probs(test_prob_predict_combined, lreg_char.classes_)

    time_interval = time() - time_interval  # Stop timer

    if verbose:
        print("\tDone in {:.2f}s".format(time_interval))
        print("****\n",
              "Combined-strategy accuracy: {:f}\n".format(accuracy_score(test_y, test_class_predict_combined)),
              "Word-based accuracy: {:f}\n".format(accuracy_score(test_y, test_class_predict_word)),
              "Char-based accuracy: {:f}\n".format(accuracy_score(test_y, test_class_predict_char)),
              "****")

    # Classification
    if verbose:
        print("\n\nStep: CLASSIFICATION.",
              "\n\t{:d} tweets to classify.".format(len(data_to_classify)))
        print("\tBegin classification.")

    time_interval = time()

    results_of_word = list()
    results_of_char = list()
    results_of_combined = list()

    # Normalizing tweets...
    data_to_classify = [" ".join(x) for x in data_to_classify]

    # Get term-document tfidf matrix
    x_word_classify = word_vec.transform(data_to_classify)
    x_char_classify = char_vec.transform(data_to_classify)

    # Get probabilities distribution for word and char predictor
    prob_predict_word = lreg_word.predict_proba(x_word_classify)
    prob_predict_char = lreg_char.predict_proba(x_char_classify)

    # Eval combined probability
    prob_predict_combined = [(x + y) * 0.5 for (x, y) in zip(prob_predict_word, prob_predict_char)]

    # Get predicted class from probability distribution
    class_predict_combined = get_classes_from_probs(prob_predict_combined, lreg_word.classes_)
    class_predict_word = get_classes_from_probs(prob_predict_word, lreg_word.classes_)
    class_predict_char = get_classes_from_probs(prob_predict_char, lreg_char.classes_)

    results_of_word.append(class_predict_word[0])
    results_of_char.append(class_predict_char[0])
    results_of_combined.append(class_predict_combined[0])

    time_interval = time() - time_interval  # Stop timer

    if verbose:
        print("\tCompleted in {:.2f}s".format(time_interval))
        print("\nPlotting data...")

    # Show results
    comparative_pie_charts([class_predict_combined, class_predict_word, class_predict_char],
                           ["combined", "word", "char"], query)
    comparative_bar_charts([class_predict_combined, class_predict_word, class_predict_char],
                           ["combined", "word", "char"], query)

    # Show features distribution
    plot_feature_distribution(x_word_classify, class_predict_word, "WORD", x_word_classify.shape[0])
    plot_feature_distribution(x_char_classify, class_predict_char, "CHAR", x_char_classify.shape[0])

    plt.show()


def knn_classifier(training_dataset, test_dataset, data_to_classify, query_string, verbose=False):
    if not isinstance(training_dataset, Dataset):
        raise IllegalArgumentError("training_dataset should be a Dataset object")
    elif not isinstance(test_dataset, Dataset):
        raise IllegalArgumentError("test_dataset should be a Databaset object")
    elif not isinstance(data_to_classify, list):
        raise IllegalArgumentError("data_to_classify should be a list object")
    elif not isinstance(query_string, str):
        raise IllegalArgumentError("query_string should be a string(str) object")

    print("******************************\nRunning KNN classifier\n******************************")

    knn = KNeighborsClassifier(n_neighbors=1)

    time_interval:float = 0.0

    # Training
    if verbose:
        print("Step: TRAINING",
              "\n\tTraining dataset has {:d} samples.".format(training_dataset.get_length()))

    time_interval = time()

    knn.fit([[data["pos_score"], data["neg_score"], data["score"]] for data in training_dataset.data["scores"]],
            training_dataset.data["labels"])

    time_interval = time() - time_interval
    if verbose:
        print("Done in {:.2f}s.".format(time_interval))

    # Test phase
    if verbose:
        print("\n\nStep: TEST.",
              "\n\tTesting dataset has {:d} samples.".format(test_dataset.get_length()))

    time_interval = time()

    test_y_predicted = knn.predict([[data["pos_score"], data["neg_score"], data["score"]] for data in test_dataset.data["scores"]])

    time_interval = time() - time_interval

    if verbose:
        print("\tCompleted in {:.2f}s".format(time_interval))
        print("\n***\nKNN accuracy: {:f}\n***".format(accuracy_score(test_dataset.data["labels"], test_y_predicted)))

    # Classify data
    if verbose:
        print("\n\nStep: CLASSIFICATION",
              "\n\t{:d} tweets to classify".format(len(data_to_classify)))
        print("\tPreparing data...")

    scores = np.array([[data["pos_score"], data["neg_score"], data["score"]]
                       for data in [training_dataset.get_tweet_score(tokens) for tokens in data_to_classify]])

    if verbose:
        print("\tBegin classification.")
    time_interval = time()

    results = knn.predict(scores)

    time_interval = time() - time_interval

    if verbose:
        print("\tDone in {:.2f}s".format(time_interval))
        print("Plotting data...")

    show_bar_pie_charts(results, query_string)
    show_histograms_bar_charts(scores, query_string)
    show_xy_score_distribution(scores, query_string)

    plt.show()


def knn_classifier_tfidf(training_dataset, test_dataset, data_to_classify, query_string, verbose=False):
    if not isinstance(training_dataset, Dataset):
        raise IllegalArgumentError("training_dataset should be a Dataset object")
    elif not isinstance(test_dataset, Dataset):
        raise IllegalArgumentError("test_dataset should be a Databaset object")
    elif not isinstance(data_to_classify, list):
        raise IllegalArgumentError("data_to_classify should be a list object")
    elif not isinstance(query_string, str):
        raise IllegalArgumentError("query_string should be a string(str) object")

    print("******************************\nRunning KNN classifier\n******************************")

    tfidf = TfidfVectorizer(ngram_range=(1, 2), analyzer='word',
                            vocabulary=lexicon.get_words_list(), stop_words=lexicon.get_stopwords())

    knn = KNeighborsClassifier(n_neighbors=1, algorithm="kd_tree", weights="uniform")

    time_interval:float = 0.0

    # Training
    if verbose:
        print("Step: TRAINING",
              "\n\tTraining dataset has {:d} samples.".format(training_dataset.get_length()))

    time_interval = time()

    if verbose:
        print("\tFitting corpus in Tf-Idf Vectorizer", end="... ")

    tfidf.fit(training_dataset.data["corpus"] + test_dataset.data["corpus"])

    if verbose:
        print("Completed.")
        print("\tExtracting features from training set", end="... ")

    training_x = tfidf.transform(training_dataset.data["corpus"])

    if verbose:
        print("Completed. Training shape: {}".format(training_x.shape))
        print("\tFitting data", end="... ")

    knn.fit(training_x, training_dataset.data["labels"])

    if verbose:
        print("Done.")

    time_interval = time() - time_interval
    if verbose:
        print("Training completed in {:.2f}s.".format(time_interval))

    # Test phase
    if verbose:
        print("\n\nStep: TEST.",
              "\n\tTesting dataset has {:d} samples.".format(test_dataset.get_length()))

    time_interval = time()

    if verbose:
        print("\tExtracting features from test set", end="... ")

    test_x = tfidf.transform(test_dataset.data["corpus"])

    if verbose:
        print("Completed. Test set shape: {}".format(test_x.shape))
        print("\tMaking predictions", end="... ")
    test_y_predicted = knn.predict(test_x)

    time_interval = time() - time_interval

    if verbose:
        print("Completed.")
        print("Test Completed in {:.2f}s".format(time_interval))
        print("***\nKNN accuracy: {:f}\n***".format(accuracy_score(test_dataset.data["labels"], test_y_predicted)))

    # Classify data
    if verbose:
        print("\n\nStep: CLASSIFICATION",
              "\n\t{:d} tweets to classify".format(len(data_to_classify)))
        print("\tPreparing data...")

    time_interval = time()
    data_to_classify = [" ".join(x) for x in data_to_classify]

    if verbose:
        print("\tExtracting features", end="... ")
    classify_x = tfidf.transform(data_to_classify)

    if verbose:
        print("Done. Features set shape: {}".format(classify_x.shape))
        print("\tClassifying data", end="... ")

    results = knn.predict(classify_x)

    time_interval = time() - time_interval

    if verbose:
        print("Done.")
        print("Classification completed in {:.2f}s".format(time_interval))
        print("Plotting data...")

    show_bar_pie_charts(results, query_string)
    plot_feature_distribution(classify_x, results, "word")

    plt.show()


def naive_bayes_classifier(training_dataset, test_dataset, data_to_classify, query_string, verbose=False):
    if not isinstance(training_dataset, Dataset):
        raise IllegalArgumentError("training_dataset should be a Dataset object")
    elif not isinstance(test_dataset, Dataset):
        raise IllegalArgumentError("test_dataset should be a Databaset object")
    elif not isinstance(data_to_classify, list):
        raise IllegalArgumentError("data_to_classify should be a list object")
    elif not isinstance(query_string, str):
        raise IllegalArgumentError("query_string should be a string(str) object")

    print("*****************************\nRunning Naive Bayes Classifier\n*****************************")

    if verbose:
        print("Step: TRAINING",
              "\n\tTraining dataset has {:d} samples".format(training_dataset.get_length()))

    start_time = end_time = 0
    if verbose:
        print("\tBegin training.")
    start_time = time()
    naive_classifier = NaiveBayesClassifier.train([[data, training_dataset.data["labels"][index]] for index, data in
                                                   enumerate(training_dataset.data["scores"])])
    end_time = time()
    if verbose:
        print("\tDone in {:.2f}s".format(end_time-start_time))

    if verbose:
        print("\n\nStep: TEST.",
              "\n\tTest dataset has {:d} samples.".format(test_dataset.get_length()))

    if verbose:
        print("\tBegin training.")
    start_time = time()
    nb_accuracy = nltk.classify.accuracy(naive_classifier, [[data, test_dataset.data["labels"][index]]
                                                            for index, data in enumerate(test_dataset.data["scores"])])
    end_time = time()
    if verbose:
        print("\tDone in {:.2f}s".format(end_time-start_time))
        print("\n***\nNaive Bayes Accuracy: {:f}\n***".format(nb_accuracy))

    if verbose:
        print("\n\nStep: CLASSIFY",
              "\n\t{:d} tweets to classify.".format(len(data_to_classify)))

    if verbose:
        print("\tBegin classification.")
    start_time = time()
    results = naive_classifier.classify_many([data for data in
                                              [training_dataset.get_tweet_score(tokens) for tokens in data_to_classify]])
    end_time = time()
    if verbose:
        print("\tDone in {:.2f}s".format(end_time-start_time))
        print("Plotting data...")

    scores = np.array([[data["pos_score"], data["neg_score"], data["score"]]
                       for data in [training_dataset.get_tweet_score(tokens) for tokens in data_to_classify]])

    show_bar_pie_charts(results, query_string)
    show_histograms_bar_charts(scores, query_string)
    show_xy_score_distribution(scores, query_string)

    plt.show()


def kmeans_classifier(training_dataset, test_dataset, data_to_classify, query_string, verbose=False):
    if not isinstance(training_dataset, Dataset):
        raise IllegalArgumentError("training_dataset should be a Dataset object")
    elif not isinstance(test_dataset, Dataset):
        raise IllegalArgumentError("test_dataset should be a Databaset object")
    elif not isinstance(data_to_classify, list):
        raise IllegalArgumentError("data_to_classify should be a list object")
    elif not isinstance(query_string, str):
        raise IllegalArgumentError("query_string should be a string(str) object")

    print("******************************\nRunning KMeans classifier\n******************************")
    kmeans = KMeans(3)

    start_time, end_time = 0, 0
    # Training
    if verbose:
        print("Step: TRAINING",
              "\n\tTraining dataset has {:d} samples.".format(training_dataset.get_length()))
    start_time = time()
    kmeans.fit([[data["pos_score"], data["neg_score"], data["score"]] for data in training_dataset.data["scores"]], training_dataset.data["labels"])
    end_time = time()
    if verbose:
        print("Done in {:.2f}s.".format(end_time-start_time))

    # Test phase
    if verbose:
        print("\n\nStep: TEST.",
              "\n\tTesting dataset has {:d} samples.".format(test_dataset.get_length()))
    start_time = time()
    test_y_predicted = kmeans.predict([[data["pos_score"], data["neg_score"], data["score"]] for data in test_dataset.data["scores"]])
    end_time = time()
    if verbose:
        print("\tCompleted in {:.2f}s".format(end_time-start_time))

    test_y_predicted, _ = get_labels_from_centers(kmeans.cluster_centers_, test_y_predicted)  # Utils function to get
                                                                                              # labels from cluster index
    if verbose:
        print("\n***\nKMeans accuracy: {:f}\n***".format(accuracy_score(test_dataset.data["labels"], test_y_predicted)))

    # Classify data
    if verbose:
        print("\n\nStep: CLASSIFICATION",
              "\n\t{:d} tweets to classify".format(len(data_to_classify)))
    scores = np.array([[data["pos_score"], data["neg_score"], data["score"]]
                       for data in [training_dataset.get_tweet_score(tokens) for tokens in data_to_classify]])

    if verbose:
        print("\tBegin classification.")
    start_time = time()
    results = kmeans.predict(scores)
    end_time = time()
    if verbose:
        print("\tDone in {:.2f}s".format(end_time-start_time))
        print("Plotting data...")

    res_labels, lookup_label = get_labels_from_centers(kmeans.cluster_centers_, results)

    show_bar_pie_charts(res_labels, query_string)
    show_3d_scatter_plot(scores, res_labels, kmeans, query_string)
    show_histograms_bar_charts(scores, query_string)
    show_xy_score_distribution(scores, query_string)
    plt.show()


def main(classifier, query, num_request=1, num_tweet=100, premium=False, product=None, dev_label=None, from_date=None, to_date=None):
    # Fetching data to classify

    if premium and isinstance(product, str) and isinstance(dev_label, str):
        fetched_tweets = twitter_api.premiumRequestPageTweet(num_request, num_tweet, query,
                                                             dev_label, product, fromDate=from_date, toDate=to_date, quiet=False)
    else:
        fetched_tweets = twitter_api.standardRequestPageTweet(num_request, num_tweet, query,
                                                              lang="it", quiet=False)

    data_to_classify = list()
    if len(fetched_tweets):

        # Load dataset
        dataset = Dataset(dataset_path, lexicon, False)
        # Split dataset
        training_dataset, test_dataset = dataset.split_training_set(0.7)

        parsed_tweets = tweet_parser.getFeatures(fetched_tweets)
        for tweet in parsed_tweets:
            data_to_classify.append(dataset.tokenize_tweet(parsed_tweets[tweet]["text"]))
    else:
        print("0 tweet fetched, no data to classify.\nExiting...")
        exit(0)



    # Execute the choosed classifier
    if classifier == 'logistic':
        logistic_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
    elif classifier == 'logistic_tfidf':
        logistic_classifier_tfidf(training_dataset, test_dataset, data_to_classify, query, be_verbose)
    elif classifier == 'nb':
        naive_bayes_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
    elif classifier == 'knn':
        knn_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
    elif classifier == 'knn_tfidf':
        knn_classifier_tfidf(training_dataset, test_dataset, data_to_classify, query, be_verbose)
    elif classifier == 'kmeans':
        kmeans_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
    elif classifier == 'all':
        logistic_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
        logistic_classifier_tfidf(training_dataset, test_dataset, data_to_classify, query, be_verbose)
        naive_bayes_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
        knn_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)
        knn_classifier_tfidf(training_dataset, test_dataset, data_to_classify, query, be_verbose)
        kmeans_classifier(training_dataset, test_dataset, data_to_classify, query, be_verbose)


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    num_request = 1
    num_tweet = 100

    config = ConfigParser()

    # Command line argument parser
    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument('--classifier', help="Indicates which classifier you want to use {}".format(classifiers), type=str, required=True)
    arg_parser.add_argument('--query', help="Search query to use to fetch twitter_api", required=True, type=str)
    arg_parser.add_argument('--num-request', help="Specify the number of tweet page to fetch.", type=int)
    arg_parser.add_argument('--num-tweet', help="Specify the number of tweet per page", type=int)
    arg_parser.add_argument('--config-file', help="Specify the name of the config file", required=False, type=str)
    arg_parser.add_argument('--verbose', help="Include the switch to have verbose output",
                            required=False, action='store_true')

    args = arg_parser.parse_args()

    be_verbose = args.verbose

    if isinstance(args.config_file, str):
        config_file = args.config_file

    if args.classifier not in classifiers:
        print("Specified an invalid classifier, valid values are: {}".format(classifiers))
        exit(2)

    if args.num_tweet and (args.num_tweet < 0 or args.num_tweet > 100):
        print("num_tweet should be 0 < num_tweet <= 100.")
        exit(2)

    if args.num_request:
        num_request = int(args.num_request)

    if args.num_tweet:
        num_tweet = int(args.num_tweet)

    # Config file parser
    config.read(config_file)
    config_sections = config.sections()
    if "keys" in config_sections:
        if "consumer_key" not in config["keys"]:
            print("Specify 'consumer_key' in 'keys' section")
            exit(2)
        else:
            consumer_key = config["keys"]["consumer_key"]

        if "consumer_key_secret" not in config["keys"]:
            print("Specify 'consumer_key_secret' in 'keys' section")
            exit(2)
        else:
            consumer_key_secret = config["keys"]["consumer_key_secret"]

        if "access_token" not in config["keys"]:
            print("Specify 'access_token' in 'keys' section")
            exit(2)
        else:
            access_token = config["keys"]["access_token"]

        if "access_token_secret" not in config["keys"]:
            print("Specify 'access_token_secret' in 'keys' section")
            exit(2)
        else:
            access_token_secret = config["keys"]["access_token_secret"]
    else:
        print("Specify 'keys' section in config file")
        exit(2)

    if "lexicon" in config_sections:
        if "type" not in config["lexicon"]:
            print("Specify 'type' option in 'lexicon' section. ('twita' or 'generic')")
            exit(2)
        elif config["lexicon"]["type"] not in ["twita", "generic"]:
            print("Correct value for 'type' option are: 'twita', 'generic'.")
            exit(2)
        if "stopwords" not in config["lexicon"]:
            print("Specify a path to stopwords with 'stopwords' option in 'lexicon' section.")
            exit(2)

        if config["lexicon"]["type"] == "generic":
            if "positive" not in config["lexicon"]:
                print("With 'generic' lexicon you should specify a 'positive' vocabulary.")
                exit(2)
            if "negative" not in config["lexicon"]:
                print("With 'generic' lexicon you should specify a 'negative' vocabulary.")
                exit(2)

            lexicon = GenericLexicon(config["lexicon"]["positive"],
                                     config["lexicon"]["negative"],
                                     config["lexicon"]["stopwords"])
        elif config["lexicon"]["type"] == "twita":
            if "dataset" not in config["lexicon"]:
                print("You have to specify the path to Twita lexicon with 'dataset' option.")
                exit(2)
            else:
                lexicon = TwITALexicon(path_to_lexicon=config["lexicon"]["dataset"],
                                       path_to_stopwords=config["lexicon"]["stopwords"])

    twitter_api = TwitterAPI(config["keys"]["consumer_key"], config["keys"]["consumer_key_secret"],
                             config["keys"]["access_token"], config["keys"]["access_token_secret"])

    tweet_parser = TweetParser()

    if "premium" in config_sections:
        from_date = to_date = None
        if "dev_label" not in config["premium"]:
            print("Specify the development label with the 'dev_label' option.")
            exit(2)
        if "product" not in config["premium"]:
            print("Specify which product to use for premium requests. ('30day'/'fullarchive')")
            exit(2)
        else:
            if config["premium"]["product"] not in ['30day', 'fullarchive']:
                print("Specified 'product' ({:.10}) is not valid, valid values are: {}".format(config["premium"]["product"],
                                                                                             ['30day', 'fullarchive']))
                exit(2)

        if "from_date" in config["premium"]:
            from_date = config["premium"]["from_date"]
        if "to_date" in config["premium"]:
            to_date = config["premium"]["to_date"]

        main(args.classifier, args.query, premium=True,
             product=config["premium"]["product"], dev_label=config["premium"]["dev_label"],
             from_date=from_date, to_date=to_date, num_request=num_request, num_tweet=num_tweet)
    else:
        main(args.classifier, args.query, num_tweet=num_tweet, num_request=num_request)




from matplotlib import pyplot as plt
from sklearn.cluster import MiniBatchKMeans as KMeans
import numpy as np

labels = ["Positive", "Neutro", "Negative"]
# Color for plot
pos_color, net_color, neg_color = '#004d00', '#b3b300', '#990000'


def comparative_bar_charts(class_predictions, predictors, query):
    plt.figure(figsize=(13, 5))
    plt.suptitle("Twitter Sentimental Analysis\nSearch query: \"{}\"".format(query))
    for index, predictions in enumerate(class_predictions):
        plt.subplot(1, 3, index + 1)
        plt.title("Result of {} preditions".format(predictors[index].capitalize()))
        pos_bar = sum([1 for pred in predictions if pred == 'positive'])
        neg_bar = sum([1 for pred in predictions if pred == 'negative'])
        net_bar = sum([1 for pred in predictions if pred == 'neutro'])

        plt.bar(labels[0], pos_bar, color=pos_color, label=labels[0])
        plt.bar(labels[1], net_bar, color=net_color, label=labels[1])
        plt.bar(labels[2], neg_bar, color=neg_color, label=labels[2])

        # Setting labels and legend
        plt.ylabel("# of tweet")
        plt.xlabel("Sentiment classes")


def comparative_pie_charts(class_predictions, predictors, query):
    plt.figure(figsize=(13, 5))
    plt.suptitle("Twitter Sentimental Analysis\nSearch query: \"{}\"".format(query))
    for index, predictions in enumerate(class_predictions):
        plt.subplot(1, 3, index + 1)
        plt.title("Result of {} predictions".format(predictors[index].capitalize()))

        pos_bar = sum([1 for pred in predictions if pred == 'positive'])
        neg_bar = sum([1 for pred in predictions if pred == 'negative'])
        net_bar = sum([1 for pred in predictions if pred == 'neutro'])

        plt.pie([pos_bar, net_bar, neg_bar],
                labels=labels,
                autopct='%1.1f%%',
                colors=[pos_color, net_color, neg_color],
                textprops=dict(color="k", weight="bold", size=10))


def plot_feature_distribution(extracted_feature, predicted_class, vectorizer_str, n_sample=None):
    if not n_sample:
        n_sample = extracted_feature.shape[0]
    elif extracted_feature.shape[0] < n_sample:
        n_sample = extracted_feature.shape[0]

    pos_features, neg_features, net_features = list(), list(), list()
    for index, prediction in enumerate(predicted_class):
        if prediction == "positive" and len(pos_features) < n_sample:
            pos_features.append(extracted_feature[index].todense().reshape((-1, 1)))
        elif prediction == "negative" and len(neg_features) < n_sample:
            neg_features.append(extracted_feature[index].todense().reshape((-1, 1)))
        elif prediction == "neutro" and len(net_features) < n_sample:
            net_features.append(extracted_feature[index].todense().reshape((-1, 1)))

        if len(pos_features) == len(net_features) == len(neg_features) == n_sample:
            break

    pos_stacked, net_stacked, neg_stacked = np.hstack(pos_features), np.hstack(net_features), np.hstack(neg_features)

    y_ticks = [round(x, 2) for x in np.arange(0, 1.01, 0.2)]
    plt.figure(figsize=(13, 5))
    plt.suptitle("TFIDF Representations of {} vectorizer".format(vectorizer_str))
    plt.subplot(1, 3, 1)
    plt.title("Positive Class")
    plt.plot(pos_stacked, color=pos_color)
    plt.yticks(y_ticks, y_ticks)
    plt.subplot(1, 3, 2)
    plt.title("Neutral Class")
    plt.plot(net_stacked, color=net_color)
    plt.yticks(y_ticks, y_ticks)
    plt.subplot(1, 3, 3)
    plt.title("Negative Class")
    plt.plot(neg_stacked, color=neg_color)
    plt.yticks(y_ticks, y_ticks)


def get_classes_from_probs(prob_list, class_list):
    # get class from probabilities array
    test_class_predict = list()
    for probs in prob_list:
        max_p = max(probs)
        for index, probability in enumerate(probs):
            if probability == max_p:
                test_class_predict.append(class_list[index])
                break

    return test_class_predict

def show_xy_score_distribution(scores, query):
    # 3 FIGURE 2 XY-Axis: Score distribution
    plt.figure(figsize=(13, 6))
    plt.suptitle("Twitter Sentiment Analysis\nSearch query: {}".format(query))
    plt.title("Score distribution")
    # 3.1 Setup Y axis ticks
    max_value = max(scores[:, 2])
    min_value = min(scores[:, 2])
    max_abs = max([abs(max_value), abs(min_value)])

    # 3.2 Plot lines
    plt.plot(scores[:, 2], '-')
    # 3.3 Setup label
    plt.ylabel("Score value")
    plt.xlabel("Sample number")
    plt.yticks([-max_abs, -int(max_abs/2), 0, int(max_abs/2), max_abs], ["Negative (-{:.2f})".format(max_abs), -int(max_abs/2), 0,
                                                                         int(max_abs/2), "Positive ({:.2f})".format(max_abs)])
    plt.hlines([-max_abs, -int(max_abs/2), 0, int(max_abs/2), max_abs], 0, len(scores), linestyles=":", alpha=0.3)


def show_bar_pie_charts(res_labels, query_string):

    # Setting window size and title
    plt.figure(figsize=(13, 6))
    plt.suptitle("Twitter Sentiment Analysis\nSearch query: '{}'".format(query_string))

    # left side - bar chart which counts positive, negative and neutro predictions
    plt.subplot(121)
    plt.title("Class distribution")
    # str list for data-labels and data heights
    labels = ["Overall", "Positive", "Neutro", "Negative"]
    pos_bar = sum([1 for x in res_labels if x == "positive"])
    neg_bar = sum([1 for x in res_labels if x == "negative"])
    net_bar = sum([1 for x in res_labels if x == "neutro"])

    # Plotting 'overall' bar
    plt.bar(labels[0], pos_bar, label=labels[0], color=pos_color)  # pos section
    plt.bar(labels[0], net_bar, label=labels[0], color=net_color, bottom=pos_bar)  # neutral section
    plt.bar(labels[0], neg_bar, label=labels[0], color=neg_color, bottom=pos_bar+net_bar)  # negative section
    # Plot positive, neutral and negative bars
    plt.bar(labels[1], pos_bar, label=labels[1], color=pos_color)  # positive bar
    plt.bar(labels[2], net_bar, label=labels[2], color=net_color)  # neutral bar
    plt.bar(labels[3], neg_bar, label=labels[3], color=neg_color)  # negative bar

    # Setting labels and legend
    plt.ylabel("# of tweet")
    plt.xlabel("Sentiment classes")
    plt.legend(labels)

    # 1.2 PIE CHART
    plt.subplot(122)
    plt.pie([pos_bar, net_bar, neg_bar],
            labels=labels[1::],
            autopct='%1.1f%%',
            colors=[pos_color, net_color, neg_color],
            textprops=dict(color="k", weight="bold", size=10))


def show_3d_scatter_plot(scores, res_labels, kmeans, query_string):
    # Colors for scatter points
    pos_color, net_color, neg_color = '#004d00', '#b3b300', '#990000'

    fig = plt.figure()
    fig.suptitle("Twitter Sentiment Analysis\nSearch query: '{}'".format(query_string))

    plt_scatter = fig.add_subplot(111, projection='3d')
    for index, xyz in enumerate(scores):
        if res_labels[index] == "positive":
            point_color = pos_color
            mark = 'P'
        elif res_labels[index] == "negative":
            point_color = neg_color
            mark = 'X'
        elif res_labels[index] == "neutro":
            point_color = net_color
            mark = 'o'

        plt_scatter.scatter(xyz[0], xyz[1], xyz[2], color=point_color, marker=mark)

    if isinstance(kmeans, KMeans):
        tmp_centroids = np.array(kmeans.cluster_centers_)
        for x, y, z in tmp_centroids:
            if z == max(tmp_centroids[:, 2]):
                plt_scatter.scatter(x, y, z, color='#00ff00', marker='P')
            elif z == min(tmp_centroids[:, 2]):
                plt_scatter.scatter(x, y, z, color='#ff1a1a', marker='X')
            else:
                plt_scatter.scatter(x, y, z, color='#ffff00', marker='o')

    plt_scatter.set_xlabel("Positive Score")
    plt_scatter.set_ylabel("Negative Score")
    plt_scatter.set_zlabel("Score")


def show_histograms_bar_charts(scores, query_string):
    # Setting window size and plot suptitle
    plt.figure(figsize=(13, 6))
    plt.suptitle("Twitter Sentiment Analysis\nSearch query: '{}'".format(query_string))

    # Bar charts
    max_y_bars = max(max(scores[:, 0]), max(scores[:, 1]))  # Set the same scale to Y-Axis
    # upper-left - positive bar chart
    plt.subplot(221)
    plt.title("Positive scores")
    # setting labels
    plt.xlabel("Sample ID")
    plt.ylabel("Score")
    # plot data
    plt.bar(range(len(scores[:, 0])), scores[:, 0], color="green")
    # setting y ticks (x ticks are sample ID)
    plt.yticks(np.arange(0, max_y_bars, 0.2))
    # upper-right - negative bar chart
    plt.subplot(222)
    plt.title("Negative scores")
    # setting labels
    plt.xlabel("Sample ID")
    plt.ylabel("Score")
    # plot
    plt.bar(range(len(scores[:, 1])), scores[:, 1], color="red")
    # setting y ticks
    plt.yticks(np.arange(0, max_y_bars, 0.2))

    # Histograms
    max_range = int(max(max(scores[:, 0]), max(scores[:, 1])))+1  # Useful to set the same x scale
    x_ticks = [x for x in np.arange(0, max_range+0.2, 0.2)]
    y_ticks = [x for x in np.arange(0, len(scores[:, 0])+1, 20)]
    num_bins = len(np.arange(0, max_range, 0.2))
    hist_range=(0, max_range)
    # bottom left - positive score distribution
    plt.subplot(223)
    # setting title and labels
    plt.title("Distribution of Positive Score")
    plt.xlabel("Positive score")
    plt.ylabel("hits number")
    # plot histogram
    plt.hist(scores[:, 0], bins=num_bins, range=hist_range, color="green")
    # setting ticks
    plt.xticks(x_ticks, rotation="vertical")
    plt.yticks(y_ticks)
    # bottom-right - negative score distribution
    plt.subplot(224)
    # setting title and labels
    plt.title("Distribution of Negative Score")
    plt.xlabel("Negative score")
    plt.ylabel("hits number")
    # plot
    plt.hist(scores[:, 1], bins=num_bins, range=hist_range, color="red")
    # setting ticks
    plt.xticks(x_ticks, rotation="vertical")
    plt.yticks(y_ticks)

    plt.subplots_adjust(bottom=0.1, left=0.1, right=0.95, wspace=0.25, hspace=0.5)


def get_labels_from_centers(kmeans_centers, kmeans_predictions):
    # Assign labels to cluster number
    lookup_int_to_labels = dict()
    centers = np.array(kmeans_centers)
    for index, center in enumerate(centers):
        if center[2] == min(centers[:, 2]):
            lookup_int_to_labels[index] = "negative"
        elif center[2] == max(centers[:, 2]):
            lookup_int_to_labels[index] = "positive"
        else:
            lookup_int_to_labels[index] = "neutro"

    labelled_predictions = list()
    for prediction in kmeans_predictions:
        labelled_predictions.append(lookup_int_to_labels[prediction])

    return labelled_predictions, lookup_int_to_labels

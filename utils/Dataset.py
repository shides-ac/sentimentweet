import csv
import re
import pandas as pd

from nltk.tokenize import RegexpTokenizer
from copy import copy
from utils.exceptions import WordNotFound
from utils.Lexicons import TwITALexicon, GenericLexicon


class Dataset:

    """
    This class loads tweet from a csv, its path is specified as parameter to constructor.
    Each entry in CSV file need to be formatted has follow:
    idtwitter, subj, opos, oneg, iro, lpos, lneg, top, text
    """

    __positive_class_name__ = "positive"
    __positive_class_indicator__ = 1
    __negative_class_name__ = "negative"
    __negative_class_indicator__ = -1
    __neutro_class_name__ = "neutro"
    __neutro_class_indicator__ = 0

    def __init__(self, path_to_dataset, lexicon, quiet):
        self.lexicon = lexicon
        self.path_to_dataset = path_to_dataset
        self.data = None

        self.__load_data__(quiet)

    def __load_data__(self, quiet=True):
        """
        Loads data from the specified CSV file in an Numpy array.
        The specified CSV should be with the following format:
        twitterid, subjectivity, opos, oneg, iro, lpos, lneg, topic, text

        Guidelines about original dataset can be found here:
        http://www.di.unito.it/~tutreeb/sentipolc-evalita16/sentipolc-guidelines2016UPDATED130916.pdf

        :return: a dictionary with three keys:
            - "tokens" which point to a list of word (tokenized version of tweet)
            - "score" a dict with "pos_score", "neg_score" and "score" keys
            - "labels" which indicates the polarity
        """

        dataset = dict()
        dataset["tokens"] = list()
        dataset["labels"] = list()
        dataset["scores"] = list()
        dataset["corpus"] = list()

        num_of_entries = len(open(self.path_to_dataset).readlines())
        print("Loading dataset...\nProcessed {}/{}".format(0, num_of_entries))
        with open(self.path_to_dataset, "r") as file_stream:
            for index, entry in enumerate(csv.reader(file_stream)):
                if entry[8] == "text":  # Exclude csv header
                    continue

                if not quiet:  # Print status
                    if int(index % 500) == 0:
                        print("Processed {}/{}".format(index, num_of_entries))

                tokens = self.tokenize_tweet(entry[8])

                if len(tokens) > 0:  # Process only un-empty tokens
                    token_scores = self.get_tweet_score(tokens)

                    normalized_tweet = " ".join(tokens)

                    dataset["tokens"].append(tokens)
                    dataset["scores"].append(token_scores)
                    dataset["corpus"].append(normalized_tweet)

                    #if entry[2] == "1" and entry[3] == "0":
                    #    dataset["labels"].append(self.__positive_class_name__)
                    #elif entry[2] == "0" and entry[3] == "1":
                    #    dataset["labels"].append(self.__negative_class_name__)
                    #else:
                    #    if entry[5] == "1" and entry[6] == "0":
                    #        dataset["labels"].append(self.__positive_class_name__)
                    #    elif entry[5] == "0" and entry[6] == "1":
                    #        dataset["labels"].append(self.__negative_class_name__)
                    #    else:
                    #        dataset["labels"].append(self.__neutro_class_name__)

                    if token_scores["score"] > 0.1:
                        dataset["labels"].append(self.__positive_class_name__)
                    elif token_scores["score"] < -0.1:
                        dataset["labels"].append(self.__negative_class_name__)
                    else:
                        dataset["labels"].append(self.__neutro_class_name__)


            if not quiet:
                print("Processed {}/{}\nDataset loaded.".format(num_of_entries, num_of_entries))

        self.data = dataset

    def get_length(self):
        """
        This function returns the dimension of the dataset.
        """
        return len(self.data["labels"])

    def split_training_set(self, percent_train):
        """
        Split the dataset into two "sub" dataset: training dataset and test dataset.

        Parameter:
            percent_train -- a float value between 0 and 1

        Returns:
            a tuple of two Dataset object: (training_dataset, test_dataset)
        """


        split_index = int(len(self.data["tokens"])*percent_train)  # Calculate the index

        training_data = dict(tokens=self.data["tokens"][0:split_index],
                             labels=self.data["labels"][0:split_index],
                             scores=self.data["scores"][0:split_index],
                             corpus=self.data["corpus"][0:split_index])
        test_data = dict(tokens=self.data["tokens"][split_index::],
                         labels=self.data["labels"][split_index::],
                         scores=self.data["scores"][split_index::],
                         corpus=self.data["corpus"][split_index::])

        training_dataset = copy(self)
        test_dataset = copy(self)

        training_dataset.data = training_data
        test_dataset.data = test_data

        return training_dataset, test_dataset

    def tokenize_tweet(self, tweet_text):
        """
        Get a tokenized version of the tweet. Remove stopwords too.

        :param tweet_text: The text to tokenize
        :return: a list of string (string token)
        """
        tweet_text = re.sub('\w+:/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', tweet_text)  # Remove URL from Tweet
        tweet_text = re.sub('\d+\s|\s\d+\s|\s\d+', '', tweet_text)  # Remove "4512 " or " 4564 " or " 4564" at the end
        tweet_text = re.sub('@\w+', "", tweet_text)

        sanitized_tweet_text = tweet_text.replace(".", " . ")\
                                         .replace(",", " , ")\
                                         .replace("?", " ? ")\
                                         .replace("!", " ! ")\
                                         .replace("-", " - ")\
                                         .replace(":", " : ")\
                                         .replace("'", " ' ")\
                                         .replace("@", "")\
                                         .replace("#", "")  # Remove @ and # (mentions and hashtag symbols)

        tokenizer = RegexpTokenizer(r'\w+')
        tokens = tokenizer.tokenize(sanitized_tweet_text)

        return [w.lower() for w in tokens if
                w.lower() not in set(self.lexicon.get_stopwords()+["http", "https", "rt"])
                and len(w) > 1]

    def get_tweet_score(self, tokens):
        pos_score = 0.0
        neg_score = 0.0
        overall_score = 0.0
        for word in tokens:
            try:
                word_stat = self.lexicon.get_word_scores(word)
                pos_score += word_stat["pos_score"]
                neg_score += word_stat["neg_score"]
                overall_score += word_stat["score"]
            except WordNotFound:
                pass

        return dict(pos_score=pos_score,
                    neg_score=neg_score,
                    score=overall_score)

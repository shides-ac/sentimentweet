from utils.exceptions import IllegalArgumentError


class TweetParser:
    """
    TweetParser class is developed to extract interesting features from Twitter's Tweet Object
    """

    def __init__(self):
        self.__exception_str__ = "TweetParser, method: \"{}\"\nMessage: {}\nError: {}"

    def getFeatures(self, tweet_objects):
        """
        The function extracts features from Twitter's Tweet object and returns it in a list.

        :param tweet_objects: the dict returned by the Twitter API
        :type tweet_objects: dict
        :return: a list of dict which represent the extracted features from tweets. Each dict has this keys:
            - "id" Tweet ID
            - "text" Tweet text, the parser extract always the full_text property
            - "geoinfo" the name of the Twitter Place if present, otherwise set as None.
            - "user" a dictionary with the following keys:
                - "id" ID of the user which make the tweet
                - "lang" the language specified by the user
                - "location" the location specified by the user, None otherwise.
        :rtype: list of { id: ... ,
                          text: ... ,
                          geoinfo: ... ,
                          user: {
                             id: ... ,
                             lang: ... ,
                             location: ... ,
                          }
                        }
        """
        if isinstance(tweet_objects, list):
            return self.__get_features_from_list__(tweet_objects)
        elif isinstance(tweet_objects, dict):
            return self.__get_features_from_tweet__(tweet_objects)
        else:
            raise IllegalArgumentError(self.__exception_str__.format("__init__",
                                                                     "You should pass a list of/single Tweet object",
                                                                     "\"tweet_objects\" should be a list or dict"))

    def __get_features_from_tweet__(self, tweet_object):
        """Extracts features from a single tweet exploiting __get_features_from_list__ function."""
        fake_list = list()

        fake_list.append(tweet_object)
        features = self.__get_features_from_list__(fake_list)
        return features[tweet_object["id"]]

    def __get_features_from_list__(self, tweet_object_list):
        features_dict = dict()

        for tweet in tweet_object_list:
            # Check if features for Tweet ID are already in dictionary
            if not features_dict.get(tweet["id"]):
                features_dict[tweet["id"]] = dict()  # Insert a new key in dictionary

                tweet_features = dict()

                tweet_keys = tweet.keys()  # Single call to .keys() method


                # Extracting user info
                tweet_features["user"] = dict()  # Extract User ID, lang and location (if available)
                if "user" in tweet_keys:
                    tweet_features["user"]["id"] = tweet["user"].get("id")
                    tweet_features["user"]["lang"] = tweet["user"].get("lang")
                    tweet_features["user"]["location"] = tweet["user"].get("location")
                else:
                    tweet_features["user"] = None

                # Extracting Tweet Text feature
                if "retweeted_status" in tweet_keys:  # In case of a retweet add the text of retweeted tweet
                    if tweet["retweeted_status"]["truncated"]:
                        tweet_features["text"] = tweet["retweeted_status"]["extended_tweet"]["full_text"]
                    else:
                        if "full_text" in tweet["retweeted_status"].keys():
                            tweet_features["text"] = tweet["retweeted_status"]["full_text"]
                        else:
                            tweet_features["text"] = tweet["retweeted_status"]["text"]
                elif tweet["is_quote_status"] and "quoted_status" in tweet_keys:  # In case of quoted status (rt + text)
                    # Add comment to quoted status as text features
                    if tweet["truncated"]:
                        tweet_features["text"] = tweet["extended_tweet"]["full_text"]
                    else:
                        if "full_text" in tweet_keys:
                            tweet_features["text"] = tweet["full_text"]
                        else:
                            tweet_features["text"] = tweet["text"]

                    # Add quoted status to dictionary
                    features_dict[tweet["quoted_status"]["id"]] = self.__get_features_from_tweet__(tweet["quoted_status"])
                elif tweet["truncated"] and "extended_tweet" in tweet_keys:  # Single Tweet, no RT or QT
                    tweet_features["text"] = tweet["extended_tweet"].get("full_text")
                else:
                    if "full_text" in tweet_keys:
                        tweet_features["text"] = tweet.get("full_text")
                    else:
                        tweet_features["text"] = tweet.get("text")

                # Extracting 'place' name if available
                if "place" in tweet_keys and tweet.get("place"):
                    tweet_features["geoinfo"] = tweet["place"].get("name")

                features_dict[tweet["id"]] = tweet_features

        return features_dict

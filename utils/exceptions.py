class WordNotFound(ValueError):
    """
    Exception raised when user request a word
    that isn't in the wordlist
    """
    pass


class EmptyKey(Exception):
    """
    Raised when there's at least one key that isn't initialized
    """


class AuthError(Exception):
    """
    Raised on Authentication/Authorization error
    """


class NotAuthenticated(Exception):
    """
    Raised when user tries to use API without authentication
    """


class IllegalArgumentError(ValueError):
    """
    Raised when user pass an illegal parameter type.
    """
    pass

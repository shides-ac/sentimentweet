import requests as req
import json, numbers
from base64 import b64encode
from time import time
from utils.exceptions import AuthError, NotAuthenticated, EmptyKey


class TwitterAPI:
    """This module gives a way to use Twitter's search API both standard and premium."""

    def __init__(self, consumer_key = None, consumer_secret_key = None, access_token = None, access_secret_token = None):
        """Init private class member and authenticate keys to obtain a bearer token.

        Keyword arguments:
            consumer_key  -- Application API key
            consumer_secret_key -- Application API secret key
            access_token -- Application Access Token
            access_secret_token -- Application Access Token Secret

        Raise:
            AuthError -- if an error occurs during authorization
        """

        # Init private class members
        # URI to Twitter's API endpoints...
        self.__standard_api_uri__ = "https://api.twitter.com/1.1/search/tweets.json?tweet_mode=extended"
        self.__premium_api_uri__ = "https://api.twitter.com/1.1/tweets/search/{}/{}.json?"

        # Bool variable to maintain authentication state of keys...
        self.__authenticated__ = False

        # Application keys...
        self.__consumer_key__ = consumer_key
        self.__consumer_secret_key__ = consumer_secret_key
        self.__access_token__ = access_token
        self.__access_secret_token__ = access_secret_token

        # Bearer token...
        self.__bearer_token__ = None

        # Request authentication token
        result, token = self.authenticate()
        if result:
            self.__bearer_token__ = token
            self.__authenticated__ = True
        else:
            raise AuthError("Error during authorization, details:\n{}".format(token))

    def authenticate(self):
        """Execute the Twitter authentication process to obtain a bearer token.

        Returns a tuple:
            first element -- Bool; True -> No Error, False -> Error
            second element -- String; bearer_token or error string

        Raise:
            EmptyKey -- if keys wasn't correctly initialized

        """
        # Check if all keys are setted
        if (self.__consumer_key__ is None) or (self.__consumer_secret_key__ is None) \
                or (self.__access_token__ is None) or (self.__access_secret_token__ is None):
            raise EmptyKey("You should init consumer key, consumer secret key, access token and access secret token in order to authenticate.")

        # Create token request
        bearer_token_req = self.__consumer_key__ + ":" + self.__consumer_secret_key__
        encoded_bearer = b64encode(bearer_token_req.encode())

        # Create request
        header = {'Authorization': 'Basic ' + encoded_bearer.decode(),
                  'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
        body = {'grant_type': 'client_credentials'}

        resp = req.post('https://api.twitter.com/oauth2/token', headers=header, data=body)
        if resp.status_code == req.codes.ok:
            return True, resp.json()['access_token']
        else:
            return False, resp.text

    def premiumSearch(self, label, product, query, from_date=None, to_date=None, max_results=None, next_param = None):
        """Perform a Twitter search based on premium channel.

        Arguments:
            label -- Development environment associated with the application
            product -- '30day' or 'fullarchive'
            query -- Query string

        Keyword arguments:
             from_date -- A string representing the starting date with the following format YYYYMMDDHHMM
             to_date -- A string representing the maximum date of tweets with the following format YYYMMDDHHMM
             max_results -- Integer which indicates the max number of tweets to retrieve
             next_param -- Used to do 'paginate' tweet request

        Returns a tuple:
            first element -- Bool; True -> No Error
                                   False -> Error
            second element -- List; List of Tweet object -> No Error
                              String; Error description -> Error
            third element -- int; The returned 'next' parameter -> No Error
                             Bool; False -> Error

        Raise:
            Exception -- Malformed or errors in passed parameter
            NotAuthenticated -- If application keys wasn't authenticated before
        """
        # Check the correctness of parameters
        if not product or ((product != "30day") and (product != "fullarchive")):
            raise Exception("Accepted values for 'product' parameter are: '30day' or 'fullarchive' (you passed {:.5}...).".format(product))
        elif not query or query == "" :
            raise Exception("'query' parameter is mandatory.")
        elif max_results and type(max_results) is not int:
            raise Exception("'maxResults' should be an Integer (you passed {:.5}...)".format(max_results))
        elif not label or label == "":
            raise Exception("'label' parameter is mandatory. Label should be valued as your Twitter development environment associated with the application.")
        elif from_date and len(from_date) != 12:
            raise Exception("Specified 'fromDate' isn't of the correct length. It should be of the following format: YYYYMMDDHHMM.")
        elif to_date and len(to_date) != 12:
            raise Exception("Specified 'toDate' isn't of the correct length. It should be of the following format: YYYYMMDDHHMM.")

        # Check if key are authenticated
        if not self.__isAuthenticated__():
            raise NotAuthenticated("Using premiumSearch without authenticated API, you should run 'authenticate' method before.")

        req_url = self.__premium_api_uri__.format(product,label)

        header = {"Authorization": "Bearer {}".format(self.__bearer_token__)}

        body = dict()
        body['query'] = query
        if from_date: body['fromDate'] = from_date
        if to_date: body['toDate'] = to_date
        if max_results: body['maxResults'] = max_results
        if next_param: body['next'] = next_param

        resp = req.post(req_url, headers=header, data=json.dumps(body))
        if resp.status_code == req.codes.ok:
            resp_json = resp.json()
            tweets, next_resp = resp_json.get("results"), resp_json.get("next")
            return True, tweets, next_resp

        else:
            return False, resp.json(), False

    def standardSearch(self, query, geocode=None, lang=None, result_type=None, count=None, until=None, since_id=None, max_id=None):
        """Perform a Twitter standard search.

        Arguments:
            query -- Query string

        Keyword arguments:
            geocode -- Filters tweets with geocode information. Information should be passed
                        with the following format ”latitude,longitude,radius“. Radius need to be expressed in
                        kilometers(km) or miles(mi).
            lang -- Restricts tweets to the given language, given by an ISO 639-1 code.
            result_type -- Specifies what type of search results you would prefer to receive.
                            Allowed values are:
                                - 'mixed' - include popular and real time results
                                - 'recent' - return only most recent results
                                - 'popular' - return the most popular tweet in the response
            count -- The number of tweets to return, up to a maximum of 100.
            until -- A string representing a date, useful to returns tweets created before the given date.
                     The date must be expressed with the following format: YYYY-MM-DD
            since_id -- int or str to specify the minimum ID of retrieved tweets. Useful to fetch only tweet with an
                        ID greater than (more recent than) 'since_id'.
            max_id -- int or str to specity the maximum ID of retrieved tweets. This is used to fetch only tweet with an
                        ID less than (less recent than) or equal to 'max_id'.

        Returns a tuple:
            first element -- Bool; True -> No Error
                                   False -> Error
            second element -- List; List of Tweet object -> No Error
                              String; Error description -> Error
            third element -- int; Twitter search metadata object -> No Error
                             Bool; False -> Error

        Raise:
            Exception -- Malformed or errors in passed parameter
            NotAuthenticated -- If application keys wasn't authenticated before
        """
        # Check parameters correctness
        if len(query) > 500:
            raise Exception("query string must be less than 500 characters")
        elif query == "":
            raise Exception("query string must not be empty")
        elif since_id and (type(since_id) is not int and type(since_id) is not str):
            raise Exception("since_id must be an integer or string")
        elif max_id and (type(max_id) is not int and type(max_id) is not str):
            raise Exception("max_id must be an integer")
        elif result_type and (result_type != "mixed") and (result_type != "popular") and (result_type != "recent"):
            raise Exception("Allowed values for 'result_type' parameter are: {}, {}, {} (you passed {:.5}...).".format('mixed', 'popular', 'recent', result_type))
        elif count and (type(count) is not int or count > 100):
            raise Exception("Parameter 'count' must be an integer < 100.")
        elif until and (type(until) is not str or len(until) != 14):
            raise Exception("Parameter 'until' must be an string representing a date in the following format: YYYY-MM-DD")
        elif geocode and (not isinstance(geocode, str) or (geocode[-2:] != "mi" and geocode[-2:] != "km")):
            raise Exception("""Parameter 'geocode' must be an string with the following format ”latitude,longitude,radius“,
            where radius units must be specified as either ”mi”(miles) or ”km”(kilometers).""")

        # Check if key are authenticated
        if not self.__isAuthenticated__():
            raise NotAuthenticated("Using standardSearch without authentication, you should run 'authenticate' method before.")

        headers = {
            "authorization": "Bearer {}".format(self.__bearer_token__)
        }

        params = dict()
        params['q'] = query
        if geocode: params['geocode'] = geocode
        if lang: params['lang'] = lang
        if result_type: params['result_type'] = result_type
        if count: params['count'] = count
        if until: params['until'] = until
        if since_id: params['since_id'] = since_id
        if max_id: params['max_id'] = max_id

        resp = req.get(self.__standard_api_uri__, headers=headers, params=params)
        if resp.status_code == req.codes.ok:
            tweets, search_metadata = resp.json()['statuses'], resp.json()['search_metadata']
            return True, tweets, search_metadata
        else:
            return False, resp.json(), False

    def standardRequestPageTweet(self, num_page, tweet_per_page, query, geocode=None, lang=None, result_type=None, since_id=None, max_id=None, until=None, quiet=True):
        """Do num_page times a Twitter standard search and returns a list of Tweet object.

        Arguments:
            num_page -- Number of request to do
            tweet_per_page -- Number of tweet to fetch in each request
            query -- Query string
            label -- Development environment associated with the application
            product -- '30day' or 'fullarchive'

        Keyword arguments:
            geocode -- Filters tweets with geocode information. Information should be passed
                        with the following format ”latitude,longitude,radius“. Radius need to be expressed in
                        kilometers(km) or miles(mi).
            lang -- Restricts tweets to the given language, given by an ISO 639-1 code.
            result_type -- Specifies what type of search results you would prefer to receive.
                            Allowed values are:
                                - 'mixed' - include popular and real time results
                                - 'recent' - return only most recent results
                                - 'popular' - return the most popular tweet in the response
            since_id -- int or str to specify the minimum ID of retrieved tweets. Useful to fetch only tweet with an
                        ID greater than (more recent than) 'since_id'.
            max_id -- int or str to specity the maximum ID of retrieved tweets. This is used to fetch only tweet with an
                        ID less than (less recent than) or equal to 'max_id'.
            until -- A string representing a date, useful to returns tweets created before the given date.
                     The date must be expressed with the following format: YYYY-MM-DD

        Returns a list:
            The returned list is a list of Tweet objects.
            Tweet object specification can be found at:
            - Tweet object:
            https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json.html#tweetobject
            - Extended Tweet object:
            https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json.html#extendedtweet

        Raise:
            Exception -- Malformed or errors in passed parameter
        """
        # Check parameters correctness
        if (not isinstance(query, str)) or (len(query) > 500 or query == ""):
            raise Exception("Query must be a string less than 500 characters and should not be empty.")
        elif since_id and (not isinstance(since_id, numbers.Number) and not isinstance(since_id, str)):
            raise Exception("'since_id' parameter must be an int or str type.")
        elif max_id and (not isinstance(max_id, numbers.Number) and not isinstance(max_id, str)):
            raise Exception("'max_id' parameter must be an int or str type.")
        elif result_type and (not isinstance(result_type, str) or (result_type != "mixed" and result_type != "popular" and result_type != "recent")):
            raise Exception("'result_type' parameter must be str type and allowed values are: \n- {}\n- {}\n- {}\n(you passed {:.5}...).".format('mixed', 'popular', 'recent', result_type))
        elif not isinstance(tweet_per_page, numbers.Number) or tweet_per_page > 100:
            raise Exception("'tweet_per_page' must be int type and maximum allowed value is 100.")
        elif until and (not isinstance(until, str) or len(until) != 10):
            raise Exception("'until' parameter must be str type, it represents a date in the following format: YYYY-MM-DD")
        elif geocode and (not isinstance(geocode, str) or (geocode[-2:] != "mi" and geocode[-2:] != "km")):
            raise Exception("""'geocode' parameter must be str type with the following format ”latitude,longitude,radius“.
            For radius must be specified an measure unit ”mi”(miles) or ”km”(kilometers).""")
        elif not isinstance(num_page, numbers.Number):
            raise Exception("'num_page' parameter must be an int str.")

        if not quiet:
            print("Fetching Tweets...")

        all_tweets = list()
        ov_start_time, ov_end_time = time(), 0
        for i in range(num_page):
            if not quiet:
                print("Request {}/{} -".format(i+1, num_page), end=" ")

            req_start_time, req_end_time = 0, 0
            req_start_time = time()
            result, tweets_of_page, metadata = self.standardSearch(query, geocode, lang, result_type, tweet_per_page, until, since_id, max_id)
            req_end_time = time()
            if result and len(tweets_of_page) > 0:
                if not quiet:
                    print("fetched {} tweets".format(len(tweets_of_page)), end=" ")

                for tweet in tweets_of_page:
                    all_tweets.append(tweet)
                max_id = tweets_of_page[-1]["id"]-1
                if since_id and max_id < int(since_id):
                    break
            elif not result:
                if not quiet:
                    print("error -> ",tweets_of_page['error']['message'], end=" ")
            else:
                if not quiet:
                    print("empty page.", end=" ")

            if not quiet:
                print("(in {:.2f}s)".format(req_end_time-req_start_time))

        if not quiet:
            print("Complete.")

        ov_end_time = time()
        if not quiet:
            print("Process completed in {:.2f}s".format(ov_end_time-ov_start_time))

        return all_tweets

    def premiumRequestPageTweet(self, num_page, tweets_per_page, query, label, product, fromDate=None, toDate=None, quiet=False):
        """Do num_pages times a Twitter premium search and returns a list of Tweet objects.

        Arguments:
            num_page -- Number of premium request to do
            tweets_per_page -- Number of tweet to fetch in each tweet request
            query -- Query string
            label -- Development environment associated with the application
            product -- '30day' or 'fullarchive'

        Keyword arguments:
            from_date -- A string representing the starting date with the following format yyyyMMddHHmm
            to_date -- A string representing the maximum date of tweets with the following format yyyyMMddHHmm

        Returns a list:
            The returned list is a list of Tweet object.
            Tweet object specification can be found at:
            - Tweet object:
            https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json.html#tweetobject
            - Extended Tweet object:
            https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json.html#extendedtweet

        Raise:
            Exception -- Malformed or errors in passed parameter
        """
        if not num_page or num_page < 1:
            raise Exception("Parameter 'num_page' is mandatory and should be greater than 0.")
        elif not query or query == "":
            raise Exception("Parameter 'query' is mandatory, empty string is invalid.")
        elif not label or label == "":
            raise Exception("Parameter 'label' is mandatory, set it as developer environment in Twitter.")
        elif not product or (product != "30day" and product != "fullarchive"):
            raise Exception("Parameter 'product' is mandatory and valid values are: \"30day\" or \"fullarchive\"")
        elif tweets_per_page == 0 or type(tweets_per_page) is not int:
            raise Exception("Parameter 'tweets_per_page' should be an Integer (you passed {:.5}...).".format(tweets_per_page))
        elif fromDate and len(fromDate) != 12:
            raise Exception("Specified 'fromDate' isn't of the correct length. It should be of the following format: yyyyMMddHHmm.")
        elif toDate and len(toDate) != 12:
            raise Exception("Specified 'toDate' isn't of the correct length. It should be of the following format: yyyyMMddHHmm.")

        if not quiet:
            print("Fetching Tweets...")

        all_tweets = list()
        next_token = None
        ov_start_time, ov_end_time = time(), 0
        for i in range(0, num_page):
            req_start_time, req_end_time = 0, 0
            if not quiet:
                print("Request {}/{} -".format(i+1, num_page), end=" ")
            req_start_time = time()
            result, tweets_of_page, next_token = self.premiumSearch(label, product, query, fromDate, toDate, tweets_per_page, next_token)
            req_end_time = time()
            if result and len(tweets_of_page):
                if not quiet:
                    print("fetched {} tweets.".format(len(tweets_of_page)), end=" ")

                for tweet in tweets_of_page:
                    all_tweets.append(tweet)
            elif not result:
                if not quiet:
                    print("error -> ", tweets_of_page['error']['message'], end=" ")
            else:
                if not quiet:
                    print("empty page.", end=" ")

            if not quiet:
                print("({:.2f})".format(req_end_time-req_start_time))

            if not next_token:
                break
        ov_end_time = time()

        if not quiet:
            print("Completed in {:.2f}s".format(ov_end_time-ov_start_time))

        return all_tweets

    def setConsumerKey(self, consumer_key = None):
        self.__consumer_key__ = consumer_key

    def setConsumerSecretKey(self, consumer_s_key = None):
        self.__consumer_secret_key__ = consumer_s_key

    def setAccessToken(self, access_token = None):
        self.__access_token__ = access_token

    def setAccessSecretToken(self, access_s_token = None):
        self.__access_secret_token__ = access_s_token

    def setBearerToken(self, bearer_token = None):
        self.__bearer_token__ = bearer_token

    def __setAuthenticated__(self, bearer_token = None):
        self.__authenticated__ = True

        self.__bearer_token__ = bearer_token

    def __isAuthenticated__(self):
        return self.__authenticated__

import csv
from utils.exceptions import WordNotFound
from utils.exceptions import IllegalArgumentError
from abc import ABC, abstractmethod


class AbstractLexicon(ABC):

    @abstractmethod
    def __init__(self):
        self.__words__ = None
        self.__stopwords__ = None
        self.__exception_str__=None

    def get_stopwords(self):
        """
        This function returns the list of stopwords
        :return: list of str object
        """

        return self.__stopwords__

    def get_words(self):
        """
        Get the entire lexicon with word's statistics too.
        :return: dict with keys "pos_score", "neg_score", "score"
        """

        return self.__words__

    def get_words_list(self):
        """
        Simply returns the list of word contained in the lexicon.
        :return: list of str object
        """

        return self.__words__.keys()

    def get_positive_words(self):
        """
        The function build a list of positive words and returns it to the user.

        :return: a list of positive words
        :rtype: list
        """
        positive_words = list()

        for word in self.__words__:
            if self.__words__[word]["score"] > 0:
                positive_words.append(word)

        return positive_words

    def get_negative_words(self):
        """
        This function builds a list of negative words and returns it.

        :return: a list of negative words
        :rtype: list
        """
        negative_words = list()

        for word in self.__words__:
            if self.__words__[word]["score"] < 0:
                negative_words.append(word)

        return negative_words

    def get_neutral_words(self):
        """
        This function builds and return a list of words considered 'unpolarized'

        :return: a list of 'unpolarized' words
        :rtype: list
        """

        neutro_words = list()

        for word in self.__words__:
            if self.__words__[word]["score"] == 0:
                neutro_words.append(word)

        return neutro_words

    def get_word_scores(self, word):
        """
        This function searches the specified word in the lexicon and, if present, returns the associated dict.

        :param word: a word or a list of words to be searched in lexicon
        :type word: str, list
        :return: the dict associated to the word ("pos_score", "neg_score", "score")
        :rtype: dict, list
        """

        if isinstance(word, str):
            try:
                return self.__words__[word]
            except (KeyError, Exception) as error:
                if isinstance(error, KeyError):
                    str_error = self.__exception_str__.format("get_word_scores",
                                                              "Word '{}' is not a key in lexicon".format(word),
                                                              "{} - {}".format(error.__class__.__name__, error),
                                                              "")
                else:
                    str_error = self.__exception_str__.format("get_word_scores",
                                                              "Error retrieving word '{}' in lexicon".format(word),
                                                              "{}".format(error),
                                                              "")
                raise WordNotFound(str_error)
        elif isinstance(word, list):
            scores = list()

            for w in word:
                scores.append(self.get_word_scores(w))

            return scores
        else:
            raise IllegalArgumentError(self.__exception_str__.format("get_word_scores",
                                                                     "Error retrieving word ('{}') in lexicon".format(word),
                                                                     "\"word\" parameter must be a string",
                                                                     ""))




class TwITALexicon(AbstractLexicon):
    """
    TwITA Lexicon is made up of a single csv file containing both positive and negative words.
    Furthermore, for each word, there's a lot of information such as pos and neg score, polarity and intensity, for
    this reason each csv entry is formatted as follow:
    word    POS(part-of-speech)     WordNetID   pos_score   neg_score   polarity    intensity

    pos_score and neg_score are defined in a way that 0 <= pos_score+neg_score <= 1 so sentiment plane is a triangle.
    Intensity is calculated as sqrt(pos_score²+neg_score²) --> (totally neutral) 0 <= Intensity <= 1 (totally polarized)
    Polarity is calculated as 1 - (4*theta)/pi where theta = atan(neg_score/pos_score); Polarity range is the following:
    (totally negative) -1 <= Polarity <= 1 (totally positive)

    TwITA Lexicon hasn't a stopwords vocabulary, if passed a path it initialize a stopwords vocabulary too.
    Stopwords vocabulary need to be 1 word per line.
    """

    def __init__(self, path_to_lexicon="Dataset/wordlist/TwITA/sentix.csv", path_to_stopwords=None):
        """
        The method initialize the class private members which maintains stopwords and words.
        Stopwords member is a simple list of word to be excluded from sentiment analysis.
        Words, instead, are maintained in an dictionary where the key is the word and the associated value is a dictionary
        with the following keys:
            -   pos_score: positive score of the word
            -   neg_score: negative score of the word
            -   score: the positive/negative weight of the word

        :param path_to_lexicon: the path to the TwITA Lexicon
        :param path_to_stopwords: the path to a generic stopwords lexicon
        :type path_to_lexicon: str
        :type path_to_stopwords: str

        :return Nothing.
        """

        print("\n\t*******************************************************************",
              "\n\t\t\t\tTwITA Lexicon",
              "\n\t\tDataset file path: {}".format(path_to_lexicon),
              "\n\t\tStopwords file path: {}".format(path_to_stopwords),
              "\n\t*******************************************************************")

        super().__init__()
        self.__exception_str__ = "LexiconTwITA, method: \"{}\".\nMessage: {}\nError: {}\n{}"

        if isinstance(path_to_stopwords, str):  # Init stopwords vocabulary
            self.__stopwords__ = list()

            try:
                with open(path_to_stopwords, "r") as stopwords_stream:
                    for stopword in stopwords_stream:
                        self.__stopwords__.append(stopword.lstrip().rstrip().replace("\n", "").lower())
            except Exception as error:
                print(self.__exception_str__.format("__init__",
                                                    "Error initializing stopwords vocabulary",
                                                    error,
                                                    "FATAL ERROR, exiting..."))
                exit(-1)
        else:
            print(self.__exception_str__.format("__init__",
                                                "Error initializing stopwords vocabulary",
                                                "\"path_to_stopwords\" must be a string",
                                                "FATAL ERROR, exiting..."))
            exit(-1)

        if isinstance(path_to_lexicon, str):  # Init TwITA lexicon
            self.__words__ = dict()

            try:
                with open(path_to_lexicon, "r") as words_stream:
                    csv_reader = csv.reader(words_stream, delimiter="\t")
                    for entry in csv_reader:
                        # Stripping space around word, stripping "_" (combined words are concatenated with "_")
                        # and lowering.
                        word = entry[0].lstrip().rstrip().replace("_"," ").lower()

                        # "score" is calculated as (10*intensity)*polarity
                        self.__words__[word] = dict(pos_score=float(entry[3]),
                                                    neg_score=float(entry[4]),
                                                    score=float(entry[6]) * float(entry[5]))
            except Exception as error:
                print(self.__exception_str__.format("__init__",
                                                    "Error initializing words dictionary",
                                                    error,
                                                    "FATAL ERROR, exiting..."))
                exit(-1)
        else:
            print(self.__exception_str__.format("__init__",
                                                "Error initializing words dictionary",
                                                "\"path_to_lexicon\" must be a string",
                                                "FATAL ERROR, exiting..."))
            exit(-1)


class GenericLexicon(AbstractLexicon):
    """
    A generic lexicon is made up of a positive, negative and stopwords vocabulary in separate text file.
    Supported vocabulary are in ".txt" format and should be present a single word per line of file.

    This kind of lexicon should be used in case of a simple sentiment analyzer based on word count, where the sentiment
    is a measure of type of word present in the statement.

    """

    def __init__(self, path_to_pos_words, path_to_neg_words, path_to_stopwords):
        """
        The method initialize the class private members which maintains stopwords and words.
        Stopwords member is a simple list of word to be excluded from sentiment analysis.
        Words, instead, are maintained in an dictionary where the key is the word and the associated value is a dictionary
        with the following keys:
            -   pos_score: positive score of the word
            -   neg_score: negative score of the word
            -   score: the positive/negative weight of the word

        :param path_to_pos_words: the path to the positive lexicon file
        :param path_to_neg_words: the path to the negative lexicon file
        :param path_to_stopwords: the path to the stopwords lexicon file
        :type path_to_stopwords: str
        :type path_to_neg_words: str
        :type path_to_pos_words: str

        :return: Nothing
        """

        print("\n\t*******************************************************************",
              "\n\t\t\t\tGeneric Lexicon",
              "\n\t\tPositive vocabulary: {}".format(path_to_pos_words),
              "\n\t\tNegative vocabulary: {}".format(path_to_neg_words),
              "\n\t\tStopwords file path: {}".format(path_to_stopwords),
              "\n\t*******************************************************************")

        super().__init__()
        self.__exception_str__ = "GenericLexicon, method: \"{}\".\nMessage: {}\nError: {}\n{}"

        if isinstance(path_to_stopwords, str):
            self.__stopwords__ = list()

            try:
                with open(path_to_stopwords, "r") as stopwords_stream:
                    for stopword in stopwords_stream:
                        self.__stopwords__.append(stopword.lstrip()
                                                          .rstrip()
                                                          .replace("\n", "")
                                                          .lower())
            except Exception as error:
                print(self.__exception_str__.format("__init__",
                                                    "Error initializing stopwords vocabulary",
                                                    error,
                                                    "FATAL ERROR, exiting..."))
                exit(-1)
        else:
            print(self.__exception_str__.format("__init__",
                                                "Error initializing stopwords vocabulary",
                                                "\"path_to_stopwords\" must be a string",
                                                "FATAL ERROR, exiting..."))
            exit(-1)

        if isinstance(path_to_pos_words, str) and isinstance(path_to_neg_words, str):
            self.__words__ = dict()

            # Adding positive words to vocabulary
            try:
                with open(path_to_pos_words, "r") as pos_words_stream:
                    for word in pos_words_stream:
                        key = word.lstrip().rstrip().replace("\n", "").lower()
                        self.__words__[key] = dict(pos_score=1,
                                                   neg_score=0,
                                                   score=1)
            except Exception as error:
                print(self.__exception_str__.format("__init__",
                                                    "Error adding positive words in lexicon",
                                                    error,
                                                    "FATAL ERROR, exiting..."))
                exit(-1)

            # Adding negative words to vocabulary
            try:
                with open(path_to_neg_words, "r") as neg_words_stream:
                    for word in neg_words_stream:
                        key = word.lstrip().rstrip().replace("\n", "").lower()
                        self.__words__[key] = dict(pos_score=0,
                                                   neg_score=1,
                                                   score=-1)
            except Exception as error:
                print(self.__exception_str__.format("__init__",
                                                    "Error adding positive words in lexicon",
                                                    error,
                                                    "FATAL ERROR, exiting..."))
                exit(-1)
        else:
            print(self.__exception_str__.format("__init__",
                                                "Error initializing stopwords vocabulary",
                                                "\"path_to_pos_words\" and \"path_to_neg_words\" must be a string",
                                                "FATAL ERROR, exiting..."))
            exit(-1)
